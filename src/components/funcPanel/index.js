import './FuncPanel.css'
import EntryForm from '../EntryForm';
import RegistForm from '../RegistForm';
import CreatePost from '../createPost';
import { useState } from 'react';
import { Button, Card } from 'antd';
import { logOut } from '../../redux/modules/auth';


const FuncPanel = ({authorizedId, token, authUserName, signIn, signUp, deleteUser, logOut}) => {

    const [signMode, setSignMode] = useState('signIn');

    const signForm = () => {
        if (signMode === "signIn") {
            return <div>
                <EntryForm authorizedId={authorizedId} signIn={signIn} setSignMode={setSignMode}/>            
            </div>
        } else {
            return <div>
                <RegistForm authorizedId={authorizedId} signUp={signUp} setSignMode={setSignMode}/>
                
            </div>
        }
    }


    const activeAccount = ( ) => {
        if (authUserName) {
            return <Card title={`Active account: ${authUserName}`}>
                <Button className='LogOutButton' onClick={() => logOut()}>
                    Sign out
                </Button>

                <Button className='DeleteUserButton' onClick={() => deleteUser(authorizedId, token)}>
                    Delete user
                </Button>

            </Card>
            
            
        } else {
            return "";
        }
    }

    const deleteButton = () => {
        if (authorizedId) {
            return 
        } else {
            return "";
        }
    }


    return <div className="FuncPanel">
        {activeAccount()}
        {signForm()}
        
        <CreatePost authorizedId={authorizedId} token={token}/>
        {deleteButton()}
    </div>
}

export default FuncPanel;