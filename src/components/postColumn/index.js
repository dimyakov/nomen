import Post from "../post";
import './PostColumn.css';

const PostColumn = ({posts, deletePost, authorizedId, token}) => {
    const renderPosts = () => {
        if (posts.length) {
            return posts.map(item => <Post key={item.id} id={item.id}
                 content={item.content} createdAt={item.createdAt} authorId={item.authorId} deletePost={deletePost}
                 authorizedId={authorizedId} token={token}/>)
        }
    }

    return <div className="PostColumn">
        {renderPosts()}
    </div>
}

export default PostColumn;