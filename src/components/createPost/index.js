import { Button, Card, Form, Input } from 'antd';
import { connect } from 'react-redux';
import { createPost as createPostAction } from '../../redux/modules/posts';

const CreatePost = ({authorizedId, createPost, token}) => {
    const onSubmit = (values) => {
        createPost(authorizedId, values.name, token);
    };

    const renderCreateForm = () => <Form
        name="basic"
        labelCol={{
        span: 8,
        }}
        wrapperCol={{
        span: 16,
        }}
        onFinish={onSubmit}
        autoComplete="off"
    >
        <Form.Item
        label="Name"
        name="name"
        rules={[
            {
            required: true,
            message: 'Please input name!',
            },
        ]}
        >
        <Input />
        </Form.Item>

        <Form.Item
        wrapperCol={{
            offset: 8,
            span: 16,
        }}
        >
        <Button type="primary" htmlType="submit">
            Submit
        </Button>
        </Form.Item>
    </Form>

    if (authorizedId) {
        return <Card>
            {renderCreateForm()}
        </Card>
       
    }
}

//export default CreatePost;

export default connect(
    null,
    {
        createPost: createPostAction
    }
)(CreatePost);
