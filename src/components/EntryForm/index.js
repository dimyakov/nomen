import { Button, Form, Card, Input } from 'antd';

const EntryForm = ({authorizedId, signIn, setSignMode}) => {
    const onSubmit = (values) => {
        signIn(values.email, values.password);
    }

    const renderForm = () => {
        return <Card title="Sign in">
        <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
        >
          <Input />
        </Form.Item>
  
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
  
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Sign in
          </Button>

          <Button onClick={() => {setSignMode("signUp")}}>
                    Sign up
           </Button>
        </Form.Item>
      </Form>
      </Card>
    };

    if (!authorizedId){
        return <div>
            {renderForm()}
        </div>
    }
    
}

export default EntryForm;