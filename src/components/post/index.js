import { Button, Card } from 'antd'
import './Post.css';

const Post = ({ id, content, createdAt, authorId, deletePost, authorizedId, token }) => {
    const renderDeleteButton = () => {
        if (authorId === authorizedId) {
            return <div className='DeleteButton'>
                <Button onClick={() => deletePost(id, token)}  type='primary'>Delete</Button>
            </div>
           
        }
    }

    return <div className='Post'>
        <Card>
            <div className='PostHeader'>
                <h1>{content}</h1>
                {renderDeleteButton()}
            </div>
            
            <span>Created at: {createdAt}</span>
        </Card>
    </div>
    
}

export default Post;