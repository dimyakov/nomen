import './App.css';
import { connect } from 'react-redux';
 
import { getPosts as getPostsAction, deletePost as deletePostAction} from './redux/modules/posts';
import { 
  signIn as signInAction,
  signUp as signUpAction,
  deleteUser as deleteUserAction,
  logOut as logOutAction 
} from './redux/modules/auth';
import PostColumn from './components/postColumn';
import { useEffect, useState } from 'react';
import FuncPanel from './components/funcPanel';


function App({ posts, getPosts, deletePost, authUserId, authUserName, authToken, signIn, signUp, deleteUser, logOut }) {
  useEffect(() => {
    getPosts();
  }, [posts])

  return (
    <div className="App">
      <div className='AppHeader'>
        Nomen - choose a name for whatever you want
      </div>
      <div className='AppMain'>
        <PostColumn posts={posts} deletePost={deletePost} authorizedId={authUserId} token={authToken}/>
        <FuncPanel authorizedId={authUserId} token={authToken} authUserName={authUserName} signIn={signIn} signUp={signUp} 
          deleteUser={deleteUser} logOut={logOut}/>
      </div>
      
    </div>
  );
}

export default connect(
  ({posts, auth}) => ({
    posts: posts.posts,
    authUserId: auth.authUserId,
    authUserName: auth.authUserName,
    authToken: auth.authToken
    }),
  {
    getPosts: getPostsAction,
    deletePost: deletePostAction,
    signIn: signInAction,
    signUp: signUpAction,
    deleteUser: deleteUserAction,
    logOut: logOutAction
  }
)(App);