import { API_URL } from '../../config';

const moduleName = 'auth';

const SIGN_IN = `${moduleName}/SIGN_IN`;
const SIGN_UP = `${moduleName}/SIGN_UP`;
const LOG_OUT = `${moduleName}/LOG_OUT`;
const DELETE_USER = `${moduleName}/DELETE_USER`
const INVALID_AUTH = `${moduleName}/INVALID_AUTH`;

const defaultState = {
    authUserId: undefined,
    authUserName: undefined,
    authToken: undefined
};

export default (state = defaultState, { type, payload }) => {
    switch (type) {
        case SIGN_IN:
            return {...state, authUserId: payload.authUserId, authUserName: payload.authUserName, authToken: payload.authToken };
        case SIGN_UP:
            return {...state, authUserId: payload.authUserId, authUserName: payload.authUserName, authToken: payload.authToken };
        case INVALID_AUTH:
            return {...state, authUserId: 'invalid', authUserName: 'invalid' };
        case DELETE_USER:
            return {...state, ...defaultState };
        case LOG_OUT:
                return {...state, ...defaultState };
        default:
            return state;
    }
}

export const signIn = (email, password) => async (dispatch) => {
    try {
        await fetch(`${API_URL}/auth/signIn/`, { 
            method: 'POST',
            
            body: JSON.stringify({
                email: email,
                password: password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
        .then((responce) => responce.json())
        .then((data) => dispatch({
            type: SIGN_IN,
            payload: {authUserId: data.id, authUserName: data.name, authToken: data.jwtToken}
        }));
    } catch (error) {
        dispatch({ type: INVALID_AUTH });
        console.log(error);
    }
}

export const signUp = (email, username, password) => async (dispatch) => {
    try {
        await fetch(`${API_URL}/auth/signUp/`, { 
            method: 'POST',
            
            body: JSON.stringify({
                email: email,
                name: username,
                password: password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
        .then((responce) => responce.json())
        .then((data) => dispatch({
            type: SIGN_UP,
            payload: {authUserId: data.id, authUserName: data.name, authToken: data.jwtToken}
        }));
    } catch (error) {
        dispatch({ type: INVALID_AUTH });
        console.log(error);
    }
}

export const deleteUser = (id, token) => async (dispatch) => {
    try {
        await fetch(`${API_URL}/users/${id}`, { 
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': `Bearer ${token}`
            }
        })
        .then((data) => dispatch({ type: DELETE_USER }));
    } catch (error) {
        dispatch({ type: INVALID_AUTH });
        console.log(error);
    }
}

export const logOut = () => async (dispatch) => {
    console.log("dsf");
    try {
        dispatch({ type: LOG_OUT });
    } catch (error) {
        dispatch({ type: INVALID_AUTH });
        console.log(error);
    }
}