import { API_URL } from '../../config';

const moduleName = 'posts';

const GET_POSTS = `${moduleName}/GET_POSTS`;
const DELETE_POST = `${moduleName}/DELETE_POST`;
const CREATE_POST = `${moduleName}/CREATE_POST`;

const defaultState = {
    posts: []
};


export default (state = defaultState, { type, payload }) => {
    switch (type) {
        case GET_POSTS:
            return {...state, posts: payload };
        case DELETE_POST:
            return {...state, posts: payload };
        case DELETE_POST:
            return {...state, posts: payload };
        default:
            return state;
    }
}

export const getPosts = () => async (dispatch) => {
    try {
        await fetch(`${API_URL}/posts`)
            .then((responce) => responce.json())
            .then((data) => dispatch({ type: GET_POSTS, payload: data }))
    } catch (error) {
        console.log(error);
    }
}

export const deletePost = (id, token) => async (dispatch) => {
    try {
        await fetch(`${API_URL}/posts/${id}`, { 
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
        dispatch({ type: DELETE_POST, payload: {id} })
    } catch (error) {
        console.log(error);
    }
}

export const createPost = (authorId, content, token) => async (dispatch) => {
    try {
        await fetch(`${API_URL}/posts/`, { 
            method: 'POST',
            
            body: JSON.stringify({
                authorId: authorId,
                content: content
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': `Bearer ${token}`
            }
        })
        .then((responce) => responce.json())
        .then((data) => dispatch({ type: CREATE_POST, payload: data }));
    } catch (error) {
        console.log(error);
    }
}