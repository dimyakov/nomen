import posts from './posts'
import auth from './auth';

export default {
    posts,
    auth
};