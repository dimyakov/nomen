import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducers from './modules';

export const configureStore = (reducers = {}, preloadedState = {}, middlewares = []) =>
    createStore(
        combineReducers({
            ...rootReducers,
            ...reducers
        }),
        preloadedState,
        compose(
            applyMiddleware(
                ...middlewares,
                thunk
            )
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );    
